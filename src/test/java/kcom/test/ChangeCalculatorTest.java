package kcom.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Properties;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

/**
 * Test for {@link ChangeCalculator}.
 *
 * @author Itso
 * @version 0.1.0
 */
public class ChangeCalculatorTest {
	private static final String FILENAME = "resources/test-coin-inventory.properties";

	private ChangeCalculator changeCalculator;

	/**
	 * Initializes object level dependencies.
	 *
	 * @throws IOException if something goes wrong during initialization.
	 */
	@Before
	public void setUp() throws IOException {

		createTestPropertiesFile(FILENAME);

		this.changeCalculator = new ChangeCalculator(FILENAME);
	}

	/**
	 * Test for {@link ChangeCalculator#getChangeFor(int)}
	 */
	@Test
	public void getOptimalChangeFor(){
		final Collection<Coin> coins = this.changeCalculator.getChangeFor(289);
		Assertions.assertThat(coins).isNotEmpty().size().isEqualTo(9);
		Assertions.assertThat(coins).contains(Coin.FIFTY_PENCE).doesNotContain(Coin.TWENTY_PENCE)
				.contains(Coin.TEN_PENCE).contains(Coin.FIVE_PENCE).contains(Coin.TWO_PENCE)
				.doesNotContain(Coin.ONE_PENNY);
	}

	private void createTestPropertiesFile(final String filename) throws IOException {
		final Properties properties = new Properties();
		properties.setProperty("100", "12");
		properties.setProperty("50", "27");
		properties.setProperty("20", "0");
		properties.setProperty("10", "120");
		properties.setProperty("5", "22");
		properties.setProperty("2", "32");
		properties.setProperty("1", "17");
		final File f = new File(filename);
		final OutputStream out = new FileOutputStream( f );
		properties.store(out, "");
	}
}