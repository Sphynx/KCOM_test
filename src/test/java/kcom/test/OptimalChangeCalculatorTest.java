package kcom.test;

import java.util.Collection;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

/**
 * Test for {@link OptimalChangeCalculator}.
 *
 * @author Itso
 * @version 0.1.0
 */
public class OptimalChangeCalculatorTest {

	private OptimalChangeCalculator optimalChangeCalculator;

	/**
	 * Initializes object level dependencies.
	 *
	 * @throws Exception if something goes wrong during initialization.
	 */
	@Before
	public void setUp() {
			this.optimalChangeCalculator = new OptimalChangeCalculator();
	}

	/**
	 * Test for {@link OptimalChangeCalculator#getOptimalChangeFor(int)}
	 */
	@Test
	public void getOptimalChangeFor(){
			final Collection<Coin> coins = this.optimalChangeCalculator.getOptimalChangeFor(89);
			Assertions.assertThat(coins).isNotEmpty().size().isEqualTo(6);
			Assertions.assertThat(coins).contains(Coin.FIFTY_PENCE, Coin.FIVE_PENCE, Coin.TWENTY_PENCE, Coin.TEN_PENCE);
	}
}