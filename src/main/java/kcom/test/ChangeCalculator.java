package kcom.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import kcom.test.file.DefaultFileHandler;
import kcom.test.file.FileHandler;

/**
 * Change calculator class.
 *
 * @author Itso
 * @version 0.1.0
 */
public class ChangeCalculator {
	private List<Coin> coins;
	private Properties properties;
	private String filename;

	private FileHandler fileHandler;

	public ChangeCalculator(final String filename) throws IOException {
		this.fileHandler = new DefaultFileHandler();
		this.coins = new ArrayList<>();
		this.filename = filename;
		this.properties = this.fileHandler.loadFromFile(this.filename);
	}

	public Collection<Coin> getChangeFor(int pence) {
		for (final Coin coin : Coin.values()) {
			pence = getCoins(pence, coin);
		}
		if(pence != 0){
			System.out.println("Error in calculation!");
		}
		this.fileHandler.saveToFile(this.filename, this.properties);
		return this.coins;
	}

	private int getCoins(final int pence, final Coin coin){
		int remainder = pence;
		Integer quantity = Integer.parseInt((String) this.properties.get(coin.denomination().toString()));
		while(remainder >= coin.denomination() && quantity > 0){
			this.coins.add(coin);
			remainder -= coin.denomination();
			quantity--;
			this.properties.setProperty(coin.denomination().toString(), quantity.toString());
		}
		return remainder;
	}
}
