package kcom.test;

/**
 * Enum for coins denomination.
 *
 * @author Itso
 * @version 0.1.0
 */
public enum Coin {
	ONE_POUND(100),
	FIFTY_PENCE(50),
	TWENTY_PENCE(20),
	TEN_PENCE(10),
	FIVE_PENCE(5),
	TWO_PENCE(2),
	ONE_PENNY(1);
	Integer denomination;
	Coin(final int denomination){
		this.denomination = denomination;
	}
	Integer denomination(){
		return this.denomination;
	}
}
