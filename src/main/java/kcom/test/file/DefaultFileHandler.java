package kcom.test.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Implementation of {@link FileHandler}.
 *
 * @author Itso
 * @version 0.1.0
 */
public class DefaultFileHandler implements FileHandler {

	@Override
	public Properties loadFromFile(final String filename) throws IOException {
		final Path configLocation = Paths.get(filename);
		try (InputStream stream = Files.newInputStream(configLocation)) {
			final Properties coinQuantities = new Properties();
			coinQuantities.load(stream);
			return coinQuantities;
		}
	}

	@Override
	public void saveToFile(final String filename, final Properties properties) {
		try {
			final File f = new File(filename);
			final OutputStream out = new FileOutputStream( f );
			properties.store(out, "");
		}
		catch (final Exception e ) {
			e.printStackTrace();
		}
	}
}
