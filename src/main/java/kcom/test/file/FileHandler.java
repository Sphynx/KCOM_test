package kcom.test.file;

import java.io.IOException;
import java.util.Properties;

/**
 * File handler interface.
 *
 * @author Itso
 * @version 0.1.0
 */
public interface FileHandler {
	Properties loadFromFile(final String filename) throws IOException;
	void saveToFile(final String filename, Properties properties);
}
