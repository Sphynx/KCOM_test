package kcom.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Optimal change calculator class.
 *
 * @author Itso
 * @version 0.1.0
 */
public class OptimalChangeCalculator {
	private List<Coin> coins = new ArrayList<>();

	public Collection<Coin> getOptimalChangeFor(int pence){
		for (final Coin coin : Coin.values()) {
			pence = getCoins(pence, coin);
		}
		if(pence != 0){
			System.out.println("Error in calculation!");
		}
		return this.coins;
	}

	private int getCoins(final int pence, final Coin coin){
		final int number = pence/coin.denomination();
		for(int i = 0; i < number; i++){
			this.coins.add(coin);
		}
		return pence%coin.denomination();
	}
}
